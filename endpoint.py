from flask import Flask, request, json, jsonify

companies = [{"id": 1, "name": "Company One"}, {"id": 2, "name": "Company Two"}]

api = Flask(__name__)


@api.route('/get', methods=['GET'])
def get():
    return json.dumps(companies)


@api.route('/get/<id>/<di>', methods=['GET'])
def get2(di, id):
    return 'teste'


@api.route('/lambda', methods=['PUT'])
def uploadReportWithLambdaParameters():
    content = request.get_json()
    print(f'Content:\n{content}')


@api.route('/lambda/2.0/repositories/<project>/<repository>/commit/<commit>/reports/<reportKey>', methods=['PUT'])
def uploadReport(project, repository, commit, reportKey):
    print(f'Project: {project}')
    print(f'Repository: {repository}')
    print(f'Commit: {commit}')
    print(f'ReportKey: {reportKey}')

    content = request.get_json()
    print(f'Content:\n{content}')


@api.route('/lambda', methods=['POST'])
def uploadAnnotationsWithLambdaParameters():
    content = request.get_json()
    print(f'Content:\n{content}')


@api.route('/2.0/repositories/<project>/<repository>/commit/<commit>/reports/<reportKey>', methods=['POST'])
def uploadAnnotations(project, repository, commit, reportKey):
    print(f'Project: {project}')
    print(f'Repository: {repository}')
    print(f'Commit: {commit}')
    print(f'ReportKey: {reportKey}')

    content = request.get_json()
    print(f'Content:\n{content}')


@api.route('/lambda', methods=['DELETE'])
def deleteExistingReportWithLambdaParameters():
    content = request.get_json()
    print(f'Content:\n{content}')


@api.route('/2.0/repositories/<project>/<repository>/commit/<commit>/reports/<reportKey>', methods=['DELETE'])
def deleteExistingReport(project, repository, commit, reportKey):
    print(f'Project: {project}')
    print(f'Repository: {repository}')
    print(f'Commit: {commit}')
    print(f'ReportKey: {reportKey}')

    content = request.get_json()
    print(f'Content:\n{content}')


if __name__ == '__main__':
    api.run()
